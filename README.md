## Transly

Python Flask Web Application demonstate Translator Consumption in Azure Congnitive Services to do the Language translation AI-ly

## Testing Environment Setup

### Create Python Virtual Environment

```
cd app
python3 -m venv .venv
source .venv/bin/activate
```

### Install application packages

```
pip3 install -r requirements.txt
```

### Create ```.env``` file

```
KEY=<Translator_key>
ENDPOINT=<Translator_API_Endpoint>
LOCATION=<Translator_Location>
```

### Start the application

```
export FLASK_ENV=development
flask run -h 0.0.0.0
```

